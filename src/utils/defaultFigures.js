const defualtFigures = [
  // dark
  {
    color: "dark",
    name: "pawn",
    horizontal: 1,
    vertical: 7,
    possibleSteps: [],
  },
  {
    color: "dark",
    name: "pawn",
    horizontal: 2,
    vertical: 7,
    possibleSteps: [],
  },
  {
    color: "dark",
    name: "pawn",
    horizontal: 3,
    vertical: 7,
    possibleSteps: [],
  },
  {
    color: "dark",
    name: "pawn",
    horizontal: 4,
    vertical: 7,
    possibleSteps: [],
  },
  {
    color: "dark",
    name: "pawn",
    horizontal: 5,
    vertical: 7,
    possibleSteps: [],
  },
  {
    color: "dark",
    name: "pawn",
    horizontal: 6,
    vertical: 7,
    possibleSteps: [],
  },
  {
    color: "dark",
    name: "pawn",
    horizontal: 7,
    vertical: 7,
    possibleSteps: [],
  },
  {
    color: "dark",
    name: "pawn",
    horizontal: 8,
    vertical: 7,
    possibleSteps: [],
  },

  // white
  {
    color: "white",
    name: "pawn",
    horizontal: 1,
    vertical: 2,
    possibleSteps: [],
  },
  {
    color: "white",
    name: "pawn",
    horizontal: 2,
    vertical: 2,
    possibleSteps: [],
  },
  {
    color: "white",
    name: "pawn",
    horizontal: 3,
    vertical: 2,
    possibleSteps: [],
  },
  {
    color: "white",
    name: "pawn",
    horizontal: 4,
    vertical: 2,
    possibleSteps: [],
  },
  {
    color: "white",
    name: "pawn",
    horizontal: 5,
    vertical: 2,
    possibleSteps: [],
  },
  {
    color: "white",
    name: "pawn",
    horizontal: 6,
    vertical: 2,
    possibleSteps: [],
  },
  {
    color: "white",
    name: "pawn",
    horizontal: 7,
    vertical: 2,
    possibleSteps: [],
  },
  {
    color: "white",
    name: "pawn",
    horizontal: 8,
    vertical: 2,
    possibleSteps: [],
  },
];

export default defualtFigures;
