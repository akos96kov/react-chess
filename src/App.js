import { useState } from "react";

import defaultFigures from "./utils/defaultFigures";

// figures
import darkPawn from "./assets/dark-pawn.png";
import whitePawn from "./assets/white-pawn.png";

import "./App.css";

const horizontal = [1, 2, 3, 4, 5, 6, 7, 8];
const vertical = [8, 7, 6, 5, 4, 3, 2, 1];

function App() {
  const [figures, setFigures] = useState(defaultFigures);

  const calculatePossibleSteps = (selectedFigure) => {
    selectedFigure.isActive = true;
    const possibleSteps = [];

    if (selectedFigure.name === "pawn") {
      if (selectedFigure.color === "dark") {
        possibleSteps.push({
          vertical: selectedFigure.vertical - 1,
          horizontal: selectedFigure.horizontal,
        });
      }

      if (selectedFigure.color === "white") {
        possibleSteps.push({
          vertical: selectedFigure.vertical + 1,
          horizontal: selectedFigure.horizontal,
        });
      }
    }

    selectedFigure.possibleSteps = possibleSteps;

    return selectedFigure;
  };

  const handleFieldClick = (hValue, vValue) => {
    const indexOfSelectedFigure = figures.findIndex(
      (figure) => figure.horizontal === hValue && figure.vertical === vValue
    );

    if (indexOfSelectedFigure !== -1) {
      // click on field with figure

      if (figures.some((figure) => figure.isActive)) {
        return;
      }

      const newFigures = [...figures];

      const figureWithPossbibleSteps = calculatePossibleSteps({
        ...newFigures[indexOfSelectedFigure],
      });

      newFigures[indexOfSelectedFigure] = { ...figureWithPossbibleSteps };

      setFigures(newFigures);
    } else {
      // click on empty field
      const activeFigureIndex = figures.findIndex((figure) => figure.isActive);

      if (activeFigureIndex === -1) {
        return;
      }

      const newFigures = [...figures];

      const selectedFigure = { ...newFigures[activeFigureIndex] };
      if (
        !selectedFigure.possibleSteps.some(
          (possibleStep) =>
            possibleStep.horizontal === hValue &&
            possibleStep.vertical === vValue
        )
      ) {
        return;
      }

      newFigures[activeFigureIndex].isActive = false;
      newFigures[activeFigureIndex].horizontal = hValue;
      newFigures[activeFigureIndex].vertical = vValue;
      newFigures[activeFigureIndex].possibleSteps = [];

      setFigures(newFigures);
    }
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        width: "100vw",
      }}
    >
      <div style={{ border: "1px solid black" }}>
        {vertical.map((vValue, vIndex) => {
          return (
            <>
              {/* <div style={{ position: "relative" }}>
                <span style={{ position: "absolute" }}>{vValue}</span>
              </div> */}
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                }}
              >
                {horizontal.map((hValue, hIndex) => {
                  return (
                    <div
                      onClick={() => handleFieldClick(hValue, vValue)}
                      style={{
                        backgroundColor:
                          (hIndex % 2 === 0 && vIndex % 2 === 0) ||
                          (vIndex % 2 === 1 && hIndex % 2 === 1)
                            ? "#b39b84"
                            : "#808a73",
                        width: "70px",
                        height: "70px",
                        border: figures.some((figure) =>
                          figure.possibleSteps.some(
                            (possibleStep) =>
                              possibleStep.vertical === vValue &&
                              possibleStep.horizontal === hValue
                          )
                        )
                          ? "1px solid red"
                          : "1px solid black",
                        cursor: "pointer",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      {figures.some(
                        (figure) =>
                          figure.horizontal === hValue &&
                          figure.vertical === vValue &&
                          figure.color === "dark"
                      ) && (
                        <img
                          alt="dark-pawn"
                          src={darkPawn}
                          style={{
                            width: figures.some(
                              (figure) =>
                                figure.horizontal === hValue &&
                                figure.vertical === vValue &&
                                figure.isActive
                            )
                              ? "76px"
                              : "70px",
                            height: figures.some(
                              (figure) =>
                                figure.horizontal === hValue &&
                                figure.vertical === vValue &&
                                figure.isActive
                            )
                              ? "76px"
                              : "70px",
                          }}
                        />
                      )}

                      {figures.some(
                        (figure) =>
                          figure.horizontal === hValue &&
                          figure.vertical === vValue &&
                          figure.color === "white"
                      ) && (
                        <img
                          alt="white-pawn"
                          src={whitePawn}
                          style={{
                            width: figures.some(
                              (figure) =>
                                figure.horizontal === hValue &&
                                figure.vertical === vValue &&
                                figure.isActive
                            )
                              ? "76px"
                              : "70px",
                            height: figures.some(
                              (figure) =>
                                figure.horizontal === hValue &&
                                figure.vertical === vValue &&
                                figure.isActive
                            )
                              ? "76px"
                              : "70px",
                          }}
                        />
                      )}
                    </div>
                  );
                })}
              </div>
            </>
          );
        })}
      </div>
    </div>
  );
}

export default App;
